Licence :
=========
  Copyright 2013-2014 Jäger Nicolas
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.



Overview :
==========
  ***«Book Of Prophecies»*** is my simulation/game engine.
  It's written for working on **linux**.  Some parts of the architecture
  are based on  the tutoiral *«Generic 3D game architecture»* wrote by
  Pierre Schwartz.

Dependecies :
==============
  - opengl
  - glfw
  - glew
  - glu
  - CEGUI

Compil :
========
  - $ cmake .
  - $ make

TODO :
=======
  - fix CEGUI

Known bug :
============
  - Alfred the beetle
  
Some links to dig :
===================
  about debugging GL :
  - http://blog.nobel-joergensen.com/2013/02/17/debugging-opengl-part-2-using-gldebugmessagecallback/?relatedposts_hit=1&relatedposts_origin=949&relatedposts_position=0

  
About the name :
================
  *«Book Of Prophecies»* comes after an episod (S04E50 *« La Réponse »*, 
  "the answer" ) of the best french tv show ever, I mean **«Kaamelott»**.

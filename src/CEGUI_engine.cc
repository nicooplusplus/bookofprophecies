/*
 * CEGUI_engine.cc
 * 
 * Copyright 2014-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include "application.hh"
#include "CEGUI_engine.hh"

CEGUI_engine::CEGUI_engine(Application* appli):engine(appli), width(640), height(480)
{
  std::cout << "initiate cegui\n";
  myRenderer = &CEGUI::OpenGLRenderer::bootstrapSystem();

  rp = static_cast<CEGUI::DefaultResourceProvider*>(CEGUI::System::getSingleton().getResourceProvider());
  rp->setResourceGroupDirectory("schemes", "GUI/schemes/");
  rp->setResourceGroupDirectory("imagesets", "GUI/imagesets/");
  rp->setResourceGroupDirectory("fonts", "GUI/fonts/");
  rp->setResourceGroupDirectory("layouts", "GUI/layouts/");
  rp->setResourceGroupDirectory("looknfeels", "GUI/looknfeel/");
  rp->setResourceGroupDirectory("lua_scripts", "GUI/lua_scripts/");
  
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
  CEGUI::Font::setDefaultResourceGroup("fonts");
  CEGUI::Scheme::setDefaultResourceGroup("schemes");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
  CEGUI::WindowManager::setDefaultResourceGroup("layouts");
  CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");
  
  guiContext = &CEGUI::System::getSingleton().getDefaultGUIContext();

  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  guiContext->getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");
  winMgr = &CEGUI::WindowManager::getSingleton();
  d_root = (CEGUI::DefaultWindow*)winMgr->createWindow("DefaultWindow", "Root");
  defaultFont = &CEGUI::FontManager::getSingleton().createFromFile("DejaVuSans-10.font");
  guiContext->setDefaultFont(defaultFont);
  guiContext->setRootWindow(d_root);

  ConsoleWindow = winMgr->loadLayoutFromFile("AlfiskoSkinConsole.layout");
  d_root->addChild(ConsoleWindow);
  
  ConsoleWindow->getChild(1/*button*/)->subscribeEvent(CEGUI::PushButton::EventClicked,CEGUI::Event::Subscriber(&CEGUI_engine::TextSubmitted,this));
  ConsoleWindow->getChild(2/*editbox*/)->subscribeEvent(CEGUI::Editbox::EventTextAccepted,CEGUI::Event::Subscriber(&CEGUI_engine::TextSubmitted,this));
  std::cout << "done\n";
}

CEGUI_engine::~CEGUI_engine()
{
}

void CEGUI_engine::process_event(engine_event& ev)
{
}

void CEGUI_engine::frame()
{
  guiContext->injectMousePosition(static_cast<float>(mouseX), static_cast<float>(mouseY));

  CEGUI::System::getSingleton().renderAllGUIContexts();
}

void CEGUI_engine::setPointerPos(double x, double y)
{
  this->mouseX=x;
  this->mouseY=y;
}

void CEGUI_engine::mouseButtonDown(const CEGUI::MouseButton& ceguiMouseButton)
{
    guiContext->injectMouseButtonDown(ceguiMouseButton);
    if( !ConsoleWindow->getChild(2/*editbox*/)->isMouseContainedInArea() ) ConsoleWindow->getChild(2/*editbox*/)->deactivate();
}

void CEGUI_engine::mouseButtonUp(const CEGUI::MouseButton& ceguiMouseButton)
{
    guiContext->injectMouseButtonUp(ceguiMouseButton);
}

void CEGUI_engine::keyDown(const CEGUI::Key::Scan&  ceguiKey)
{
    guiContext->injectKeyDown(ceguiKey);
}

void CEGUI_engine::keyUp(const CEGUI::Key::Scan&  ceguiKey)
{
    guiContext->injectKeyUp(ceguiKey);
}

void CEGUI_engine::sendChar(unsigned int chr)
{
    guiContext->injectChar(chr);
}

void CEGUI_engine::pointerVisibility(const bool visibility)
{
  if(visibility)
    guiContext->getMouseCursor().hide( );
  else
    guiContext->getMouseCursor().show( );
}

void CEGUI_engine::switchConsoleVisibility()
{
  consoleVisibility=!consoleVisibility;
  ConsoleWindow->setVisible(consoleVisibility);
}

bool CEGUI_engine::TextSubmitted(const CEGUI::EventArgs &e)
{
   this->ParseText(ConsoleWindow->getChild(2/*editbox*/)->getText());
   ConsoleWindow->getChild(2/*editbox*/)->setText("");
}

bool CEGUI_engine::commandLineIsActive(){
  return ConsoleWindow->getChild(2/*editbox*/)->isActive();
}

void CEGUI_engine::ParseText(CEGUI::String inMsg)
{

  std::string inString = inMsg.c_str();
 
	if (inString.length() >= 1) // Be sure we got a string longer than 0
	{
		if (inString.at(0) == '/') // Check if the first letter is a 'command'
		{
			std::string::size_type commandEnd = inString.find(" ", 1);
			std::string command = inString.substr(1, commandEnd - 1);
			std::string commandArgs = inString.substr(commandEnd + 1, inString.length() - (commandEnd + 1));
			//convert command to lower case
			for(std::string::size_type i=0; i < command.length(); i++)
			{
				command[i] = tolower(command[i]);
			}
 
      if (command == "pos")
			{
        //~ OutputText(get_GLengine()->cam.info(),CEGUI::Colour(1.0f,1.0f,1.0f));
			}
			else if (command == "axis")
			{
        //~ get_GLengine()->worldAxis.toggle();
			}
			else if (command == "quit")
			{
        get_parent()->stop();
			}
			else if (command == "help")
			{
        std::string outString = 
          "Let the show begin...\n"
          "/help : this message\n"
          "/axis : toggle 3D axis\n"
          "/pos  : print camera position and direction\n"
          "tab : toggle the console\n"
          "Right,Left,Up,Down keys : camera translation in the view direction\n"
          "middle button : switch between panning and cursor modes";
            
        OutputText(outString,CEGUI::Colour(1.0f,1.0f,1.0f));
			}
			else
			{
				std::string outString = "<" + inString + "> is an invalid command.";
				(this)->OutputText(outString,CEGUI::Colour(1.0f,0.0f,0.0f));
			}
		}
		else
		{
      std::string outString = "*" + inString + "*";
      (this)->OutputText(outString,CEGUI::Colour(0.96f,0.88f,0.0f));
		}
	} 
}

void CEGUI_engine::OutputText(CEGUI::String inMsg, CEGUI::Colour colour = CEGUI::Colour( 0xFFFFFFFF))
{
  CEGUI::Listbox *outputWindow = static_cast<CEGUI::Listbox*>(ConsoleWindow->getChild(3/*listbox*/));
	CEGUI::ListboxTextItem* newItem=0;
  newItem = new CEGUI::ListboxTextItem( (inMsg.append("\n "))/*(inMsg.insert(0,"\n"))*/ );
	newItem->setTextColours(colour);
	outputWindow->addItem(newItem);
}

/*
 * CEGUI_engine.hh
 * 
 * Copyright 2014-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef __CEGUI_ENGINE_H_INCLUDED__
#define __CEGUI_ENGINE_H_INCLUDED__
#include "engine.hh"

class CEGUI_engine : public engine
{
public:
  CEGUI_engine(Application*);
  ~CEGUI_engine();
  void frame();
  void setPointerPos(double,double);
  void mouseButtonDown(const CEGUI::MouseButton& );
  void mouseButtonUp(const CEGUI::MouseButton& );
  void keyDown(const CEGUI::Key::Scan& );
  void keyUp(const CEGUI::Key::Scan& );
  void sendChar(unsigned int chr);
  void pointerVisibility(const bool);
  void switchConsoleVisibility(void);
  bool TextSubmitted(const CEGUI::EventArgs&);
  bool commandLineIsActive(void);


private:
  int width, height;
  CEGUI::OpenGLRenderer*            myRenderer;
  CEGUI::DefaultResourceProvider*   rp;
  CEGUI::GUIContext*                guiContext;
  CEGUI::DefaultWindow*             d_root;
  CEGUI::WindowManager*             winMgr;
  CEGUI::Font*                      defaultFont;
  CEGUI::Window*                    ConsoleWindow; 
  bool                              consoleVisibility;    
  double                            mouseX, mouseY;   // pointer's coords
  void                              ParseText(CEGUI::String);
  void                              OutputText(CEGUI::String, CEGUI::Colour);


protected:
  void process_event(engine_event&);
};

#endif // __CEGUI_ENGINE_H_INCLUDED__

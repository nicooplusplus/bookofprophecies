/*
 * GLFW_engine.cc
 * 
 * Copyright 2014 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include "CEGUI_engine.hh"
#include "application.hh" // get_parent()
#include "GLFW_engine.hh"

GLFW_engine* GLFW_engine::callBackPtr = nullptr;

GLFW_engine::GLFW_engine(Application* appli, std::string appliWinTitle):engine(appli), width(800), height(600) // think about width and height...
{
  glfwSetErrorCallback(error_callback);
  
  if (!glfwInit())
  {
    std::cout << "glfwInit() failed" << std::endl;
    exit(EXIT_FAILURE);
  }
  callBackPtr = this; // needed because glfw is a C, not C++, lib..
  // attrib. to specify to GL
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

  if(get_parent()->parsed["fullscreen"]==true)
  {
    GLFWvidmode *mode = (GLFWvidmode*)glfwGetVideoMode(glfwGetPrimaryMonitor());
    width = mode->width;
    height=mode->height;
    get_parent()->setResolution(width,height);
    window = glfwCreateWindow(width, height, appliWinTitle.c_str(), glfwGetPrimaryMonitor(), NULL); // full screen
  }
  else
    window = glfwCreateWindow(width, height, appliWinTitle.c_str(), NULL, NULL); // windowed
  
  if (!window)
  {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }
  
  glfwMakeContextCurrent(window);
  glfwSetKeyCallback(window, key_callback);
  glfwSetMouseButtonCallback(window, button_callback);
  glfwSetCursorPosCallback(window, pointer_callback);
  glfwSetCharCallback(window, char_callback);
  glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
  glfwSetInputMode(window, GLFW_CURSOR , GLFW_CURSOR_HIDDEN);// hide the default X11 cursor when mouse is over the window
  glfwSetWindowShouldClose(window, GL_FALSE);
}

GLFW_engine::~GLFW_engine()
{
  glfwDestroyWindow(window);
  glfwTerminate();
}

void GLFW_engine::process_event(engine_event& ev)
{
}

void GLFW_engine::frame()
{
  if(glfwWindowShouldClose(window))
  {
    get_parent()->stop();
  }
  else
  {
    if ( !(glfwGetWindowAttrib(window, GLFW_VISIBLE )) )
    {
      std::cout << "not visible" << std::endl;
    }
    glfwGetFramebufferSize(window, &width, &height);
    glfwSwapBuffers(window);
    glfwPollEvents();    
  }
}

void GLFW_engine::error_callback(int error, const char* description)
{
  // TODO :: check if enum...
  std::cerr << "GLFW_engine error ("<< error << "," <<description <<")"<< std::endl;
}

void GLFW_engine::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if (action == GLFW_PRESS)
    switch(key)
    {
      case GLFW_KEY_ESCAPE:     callBackPtr->get_parent()->stop();
      break;
      
      //~ case GLFW_KEY_UP:         callBackPtr->get_GLengine()->cam.slide(FORWARD);
      //~ break;
      //~ 
      //~ case GLFW_KEY_DOWN:       callBackPtr->get_GLengine()->cam.slide(BACKWARD);
      //~ break;
      //~ 
      //~ case GLFW_KEY_RIGHT:      if(callBackPtr->get_CEGUIengine()->commandLineIsActive()) 
                                //~ callBackPtr->get_CEGUIengine()->keyDown(CEGUI::Key::ArrowRight); 
                                //~ else callBackPtr->get_GLengine()->cam.slide(STRAFERIGHT);
      //~ break;
      //~ 
      //~ case GLFW_KEY_LEFT:       if(callBackPtr->get_CEGUIengine()->commandLineIsActive())
                                //~ callBackPtr->get_CEGUIengine()->keyDown(CEGUI::Key::ArrowLeft);
                                //~ else callBackPtr->get_GLengine()->cam.slide(STRAFELEFT);
      //~ break;
      
      case GLFW_KEY_TAB:        callBackPtr->get_CEGUIengine()->switchConsoleVisibility();
      break;
            
      case GLFW_KEY_ENTER:      callBackPtr->get_CEGUIengine()->keyDown(CEGUI::Key::Return);
      break;
      
      case GLFW_KEY_DELETE:     callBackPtr->get_CEGUIengine()->keyDown(CEGUI::Key::Delete);
      break;
      
      case GLFW_KEY_HOME:       callBackPtr->get_CEGUIengine()->keyDown(CEGUI::Key::Home);
      break;
      
      case GLFW_KEY_END:        callBackPtr->get_CEGUIengine()->keyDown(CEGUI::Key::End);
      break;
      
      case GLFW_KEY_BACKSPACE:  callBackPtr->get_CEGUIengine()->keyDown(CEGUI::Key::Backspace);
      break;
      
      default:
      break;
    }
    
  if (action == GLFW_RELEASE)
    switch(key)
    {
      case GLFW_KEY_ENTER     : callBackPtr->get_CEGUIengine()->keyUp(CEGUI::Key::Return);
      break;
      
      default:
      break;
    }

}

void GLFW_engine::button_callback(GLFWwindow* window, int button, int action, int mods)
{
  if (action == GLFW_PRESS)
    switch(button)
    {
      case GLFW_MOUSE_BUTTON_3:    callBackPtr->panning=!callBackPtr->panning; callBackPtr->get_CEGUIengine()->pointerVisibility(callBackPtr->panning);
      break;
      
      case GLFW_MOUSE_BUTTON_1: callBackPtr->get_CEGUIengine()->mouseButtonDown(CEGUI::LeftButton);
      break;
      
      default:
      break;
    }
  else if (action == GLFW_RELEASE)
    switch(button)
    {      
      case GLFW_MOUSE_BUTTON_1: callBackPtr->get_CEGUIengine()->mouseButtonUp(CEGUI::LeftButton);
      break;
      default:
      case GLFW_MOUSE_BUTTON_3:    // do nothing!
      break;
    }
}

void GLFW_engine::pointer_callback(GLFWwindow* windows, double x, double y)
{
  callBackPtr->get_CEGUIengine()->setPointerPos(x,y);
}

void GLFW_engine::char_callback(GLFWwindow* windows,unsigned int chr)
{
  callBackPtr->get_CEGUIengine()->sendChar(chr);
}

GLFWwindow* GLFW_engine::getWin()
{
  return window;
}

/*
 * GLFW_engine.hh
 * 
 * Copyright 2014 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef __GLFW_ENGINE_H_INCLUDED__
#define __GLFW_ENGINE_H_INCLUDED__
#include "engine.hh"

class GLFW_engine : public engine
{
public:
  GLFW_engine(Application*, std::string = "Book of Prophecies");
  ~GLFW_engine();
  void frame();
  bool panning;
  GLFWwindow* getWin();

private:
  GLFWwindow* window;
  int width, height;

protected:
  void process_event(engine_event&);
  static void error_callback(int,const char*);
  static void key_callback(GLFWwindow*,int,int,int,int);
  static void button_callback(GLFWwindow*,int,int,int);
  static void pointer_callback(GLFWwindow*,double,double);
  static void char_callback(GLFWwindow*,unsigned int);
  static GLFW_engine* callBackPtr;


};

#endif // __GLFW_ENGINE_H_INCLUDED__

/*
 * OSG_engine.cc
 * 
 * Copyright 2014 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include "GLFW_engine.hh"
#include "OSG_engine.hh"
#include <osgDB/ReadFile>
#include <osg/MatrixTransform>

OSG_engine::OSG_engine(Application* appli):engine(appli), width(800), height(600)
{
    root_ptr = new osg::Group();
    osg::ref_ptr<osg::Node> loadedModel = osgDB::readNodeFile("cessna.osg");
    // create the view of the scene.

    //~ root_ptr->addChild( loadedModel.get() );          

    transformation = new osg::MatrixTransform();
    transformation->setMatrix( osg::Matrix::translate( 0.0f, 0.0f, -75.0f ) );
    transformation->addChild( loadedModel.get() );
    root_ptr->addChild( transformation.get() );
    
    viewer = new osgViewer::Viewer;
    window = viewer->setUpViewerAsEmbeddedInWindow(0,0,width,height);
    viewer->setSceneData( root_ptr ); // can use any node, or transformation instead of root_ptr...
    viewer->realize();
}

OSG_engine::~OSG_engine()
{
  
}

void OSG_engine::process_event(engine_event& ev)
{
}

void OSG_engine::frame()
{
  viewer->frame();
}

void OSG_engine::panningCam()
{
}

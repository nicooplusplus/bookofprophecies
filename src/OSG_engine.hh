/*
 * OSG_engine.hh
 * 
 * Copyright 2014-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef __OSG_ENGINE_H_INCLUDED__
#define __OSG_ENGINE_H_INCLUDED__
#include "engine.hh"
#include <osg/ref_ptr>
#include <osg/Camera>
#include <osg/Group>
#include <osgViewer/Viewer>



class OSG_engine : public engine
{
public:
  OSG_engine(Application*);
  ~OSG_engine();
  void frame();
  void panningCam();

private:
  int width, height;
  osg::ref_ptr<osg::Camera> cam;
  osg::ref_ptr<osgViewer::Viewer> viewer;
  osg::observer_ptr<osgViewer::GraphicsWindow> window;
  osg::ref_ptr<osg::Group> root_ptr;
  osg::ref_ptr<osg::MatrixTransform> transformation;

protected:
  void process_event(engine_event&);
};

#endif // __OSG_ENGINE_H_INCLUDED__

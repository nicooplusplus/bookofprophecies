#include "RN_engine.hh"
#include "application.hh" // get_parent()
#include <iostream>
#include <raknet/MessageIdentifiers.h>
#include <raknet/BitStream.h>

enum GameMessages
{
	ID_GAME_MESSAGE_1=ID_USER_PACKET_ENUM+1
};


#define SERVER_PORT 60000
#define SERVER_ADDR "192.168.2.12" // only lan server, not internet IP

RN_engine::RN_engine(Application* appli
              )
              :engine(appli)
{
  std::cout << "starting network..." << std::endl;
  peer = RakNet::RakPeerInterface::GetInstance();
  RakNet::SocketDescriptor sd;
  peer->Startup(1,&sd, 1);
  peer->Connect(SERVER_ADDR, SERVER_PORT, 0,0);
}

RN_engine::~RN_engine()
{
  std::cout << "stoping network..." << std::endl;
  RakNet::RakPeerInterface::DestroyInstance(peer);
}

void RN_engine::process_event(engine_event& ev)
{
}

void RN_engine::frame()
{
  for (packet=peer->Receive(); packet; peer->DeallocatePacket(packet), packet=peer->Receive())
  {
    switch (packet->data[0])
    {
      case ID_CONNECTION_REQUEST_ACCEPTED:
      {
        std::cout << "Our connection request has been accepted.\n";
        
        RakNet::BitStream bsOut;
        bsOut.Write((RakNet::MessageID)ID_GAME_MESSAGE_1);
        bsOut.Write("Hello server!");
        peer->Send(&bsOut,HIGH_PRIORITY,RELIABLE_ORDERED,0,packet->systemAddress,false);

      } break;	
      
      case ID_NO_FREE_INCOMING_CONNECTIONS:
      {
        std::cout << "The server is full.\n";
        this->get_parent()->stop();
      } break;
      
      case ID_DISCONNECTION_NOTIFICATION:
      {
        std::cout << "We have been disconnected.\n";
        this->get_parent()->stop();
      } break;
      
      case ID_CONNECTION_LOST:
      {
        std::cout << "Connection lost.\n";
        this->get_parent()->stop();
      } break;
        
      case ID_GAME_MESSAGE_1:
      {
        RakNet::RakString rs;
        RakNet::BitStream bsIn(packet->data,packet->length,false);
        bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
        bsIn.Read(rs);
        std::cout << rs << std::endl;
      } break;
        
      default:
      {
        std::cout << "Message with identifier" << packet->data[0] << " has arrived.\n";
      } break;
    }
  }
}

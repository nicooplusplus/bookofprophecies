/*
 * RN_engine.hh
 * 
 * Copyright 2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef __RN_ENGINE_H_INCLUDED__
#define __RN_ENGINE_H_INCLUDED__
#include "engine.hh"
#include <raknet/RakPeerInterface.h>

class RN_engine : public engine
{
public:
  RN_engine(Application*);
  ~RN_engine();
  void frame();
  
private:
  RakNet::RakPeerInterface *peer;
  RakNet::Packet *packet;
  
protected:
  void process_event(engine_event&);
  
};

#endif // __RN_ENGINE_H_INCLUDED__

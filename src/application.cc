/*
 * application.cc
 * 
 * Copyright 2014-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include "GLFW_engine.hh"
#include "OSG_engine.hh"
#include "CEGUI_engine.hh"
#include "RN_engine.hh"
#include "application.hh"

Application::Application(std::map<std::string, bool>& _data)
: parsed(_data)
{
  eGLFW = new GLFW_engine(this /*«,» ADD HERE THE TITLE OF THE MAIN WINDOW*/);
  eOSG = new OSG_engine(this);
	eCEGUI = new CEGUI_engine(this);
  eRN = new RN_engine(this);

	// bind engines
  eCEGUI->attach_OSG_engine(eOSG);
	eCEGUI->attach_GLFW_engine(eGLFW);
  eCEGUI->attach_RN_engine(eRN);
  
	eOSG->attach_GLFW_engine(eGLFW);
	eOSG->attach_CEGUI_engine(eCEGUI);
  eOSG->attach_RN_engine(eRN);

	eGLFW->attach_OSG_engine(eOSG);
	eGLFW->attach_CEGUI_engine(eCEGUI);
  eGLFW->attach_RN_engine(eRN);
  
  eRN->attach_CEGUI_engine(eCEGUI);
  eRN->attach_GLFW_engine(eGLFW);
  eRN->attach_OSG_engine(eOSG);
}


void Application::run(){
	bool current_still_running = still_running;
	
	// create a lock for the mutex
	boost::mutex::scoped_lock l(still_running_mutex);
	l.unlock();	
	
	while (current_still_running){
    eRN->frame();
		eGLFW->frame();
    eOSG->frame();
//    eCEGUI->frame();
		l.lock();
			current_still_running = still_running;
		l.unlock();
	}
}

void Application::stop(){
	boost::mutex::scoped_lock l(still_running_mutex);
	still_running = false;
}

Application::~Application(){
  delete eCEGUI;
	delete eOSG;
	delete eGLFW;
  delete eRN;
}

int Application::getWidth()
{
  return width;
}

int Application::getHeight()
{
  return height;
}

void Application::setResolution(const int w, const int h)
{
  width = w ; height = h;
}

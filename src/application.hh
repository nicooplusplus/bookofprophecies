/*
 * application.hh
 * 
 * Copyright 2014-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef __APPLICATION_H_INCLUDED__
#define __APPLICATION_H_INCLUDED__

#include <boost/thread/thread.hpp>

class Argparse;
class CEGUI_engine;
class GLFW_engine;
class OSG_engine;
class RN_engine;

class Application{
public:
	Application(std::map<std::string, bool>&);
	~Application();
	bool still_running;
  void run();
  void stop();
  std::map<std::string, bool> parsed; // put in private, should not be modified by engines...
  int getWidth(); // change for get resolution...
  int getHeight();
  void setResolution(const int,const int);
  
private:
	CEGUI_engine *eCEGUI;
	RN_engine *eRN;
  OSG_engine *eOSG;
	GLFW_engine *eGLFW;
  boost::mutex still_running_mutex;
  int width, height;

};


#endif // __APPLICATION_H_INCLUDED__

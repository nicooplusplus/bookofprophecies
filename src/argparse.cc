/*
 * argparse.cc
 * 
 * Copyright 2014-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "argparse.hh"
#include "iostream"
/**
 * parses the command line and store the argument into data
 */
Argparse::Argparse(int argc, char **argv){
	std::vector<std::string> command_line;
	for (int i=1; i<argc; i++)
		command_line.push_back(std::string(argv[i]));

	// set the default values
	data["fullscreen"]   = false;
	data["windowed"]     = true;
  HELP                 = false;
  ERROR                = false;
  bool DATA = false;

	// parsing
	std::vector<std::string>::iterator i;
  std::vector<std::string>::iterator j;
  
	for (i=command_line.begin(); i!=command_line.end(); i++){
    if(*i == "--help" || *i == "-h" )
    {
      HELP = true;
    }
		else if (*i == "--fullscreen" || *i == "-fs" )
    {
			data["windowed"]   = false;
      data["fullscreen"] = true;
    }
    else if(*i == "--windowed" || *i == "-w")
    {
      data["windowed"]   = true;
      data["fullscreen"] = false;
      if( i == command_line.end() )
        ERROR = true;
      else
      {
        std::cout << *(++i) << std::endl; 
        std::cout << *i << std::endl; 
          if( i == command_line.end() )
          {
            ERROR = true;
          }
          else
          {
            std::cout << *(++i) << std::endl; 
          }
      }
    }
    else
    {
      ERROR = true; // unknown argument.
    }
    if(ERROR)
    {
      std::cout << "erreur";
    }
	}
}

bool Argparse::help()
{
  if(HELP)
  {
    std::cout << "« BookOfProphecies »\n"
                 " --help, -h : this window\n"
                 " --fullscreen, -fs : fullscreen mode\n";
    return true;
  }
  else
  {
    return false;
  }
}

bool Argparse::error()
{
  if(ERROR)
  {
    std::cout << "Bad argument(s), use \"--help\" or \"-h\" to see all possible arguments.\n";
    return true;
  }
  else
  {
    return false;
  }
}

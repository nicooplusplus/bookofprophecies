/*
 * engine.cc
 * 
 * Copyright 2014-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "application.hh"
#include "engine.hh"

engine::engine(Application* Appli)
: parent(Appli), eCEGUI(0), eRN(0), eGLFW(0)
{
}

engine::~engine()
{
	// waits for all the threads to terminate
	// take care to interblockings
}

void engine::send_message_to_RN(engine_event& e){
	//~ eRN->push_event(e);
}

void engine::send_message_to_CEGUI(engine_event& e){
	//~ eCEGUI->push_event(e);
}

void engine::send_message_to_GLFW(engine_event& e){
	//~ eGLFW->push_event(e);
}

/**
 * adds en event in the TODO queue
 */
void engine::push_event(engine_event& e){
	// as well as for the process queue function, we protect the event queue with the mutec
	boost::mutex::scoped_lock lock(queue_mutex);
	events_queue.push(e);
	// the mutex is automatically unlocked at this point due to the end of the scope
}

/**
 * empties the event queue. For each event, it launches the appropriate process
 */
void engine::process_queue(){
	while (! events_queue.empty()){
		// we must prevent concurrent accesses, that's why we use scoped lock
		boost::mutex::scoped_lock lock(queue_mutex);
			engine_event &e = events_queue.front();
			events_queue.pop();
		
		// let's release the mutex
		// the mutex is released before the end of the scope in order to minimize the critical zone
		lock.unlock();

		process_event(e);
	}
}


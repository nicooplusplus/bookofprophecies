/*
 * engine.hh
 * 
 * Copyright 2014-2015 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#ifndef __ENGINE_H_INCLUDED__
#define __ENGINE_H_INCLUDED__

#include <map> // needed for engine_event
#include <queue> 
#include <boost/thread/thread.hpp>

// ALL ENGINES STUFFS. VERY IMPORTANT : TAKE CARE OF THE ORDER.
#include <GL/glew.h> // need to be added befor glfw (thanks to the FAQ of glfw)
#include <GLFW/glfw3.h>
#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GLRenderer.h>

class CEGUI_engine;
class RN_engine;
class GLFW_engine;
class Application;
class OSG_engine;

/**
* generic class to manage events between engines
*/

class engine_event
{
public:
	int type;
	std::map<std::string, std::string> s_data;
	std::map<std::string, int> i_data;

	bool operator==(const engine_event& e)
  {
		return (type == e.type) 
        && (s_data == e.s_data) 
			  && (i_data == e.i_data);
	}

	template<class Archive>
	void serialize(Archive& ar, const unsigned int)
  {
		ar & type;
		ar & s_data;
		ar & i_data;
	}
};


class engine
{
public:
	engine(Application*);
	virtual ~engine();
  void send_message_to_RN    (engine_event&);
	void send_message_to_CEGUI (engine_event&);
	void send_message_to_GLFW  (engine_event&);
  
  void attach_CEGUI_engine(CEGUI_engine *e){eCEGUI = e;}
  void attach_RN_engine(RN_engine *e)      {eRN    = e;}
  void attach_GLFW_engine(GLFW_engine *e)  {eGLFW  = e;}
  void attach_OSG_engine(OSG_engine *e)    {eOSG   = e;}
  
	inline Application*		get_parent()			{return parent;}
	inline CEGUI_engine*	get_CEGUIengine()	{return eCEGUI;}
  inline RN_engine*		  get_RNengine()		{return eRN;   }
  inline GLFW_engine*		get_GLFWengine()  {return eGLFW; }
  inline OSG_engine*		get_OSGengine()   {return eOSG; }
	
	void push_event(engine_event&); // accepts the event from previous engines
	void process_queue(); // empty the queues from previous engines
  
protected:
  CEGUI_engine *eCEGUI;
	RN_engine    *eRN;
	GLFW_engine  *eGLFW;
  OSG_engine   *eOSG;

	std::queue <engine_event> events_queue;
	Application *parent; // pointer to container object
	boost::mutex queue_mutex; // mutex to protect the event queue
  
  virtual void process_event(engine_event&) = 0;
  
};

#endif // __ENGINE_H_INCLUDED__

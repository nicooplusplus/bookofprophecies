/*
 * main.cc
 * 
 * Copyright 2013-2014 Jäger Nicolas
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include <unistd.h>
#include <iostream>
#include "argparse.hh"
#include "application.hh"

int main(int argc, char **argv)
{
  if(geteuid()==0)
  {
    std::cerr<<"ABORTED: Attempted to run with root privileges!\n";
    return EXIT_FAILURE;
  }
  
  std::cout << std::endl << std::endl
            << "- for dev :"
            << "\n old version moved to Drafts."
            << "\n if the server isn't present at start, the client will"
            << "\n not close yet."
            << "\n make the client trying for some seconds to reach a"
            << "\n the server. Else, shutdown the client"
            << std::endl << std::endl;

  Argparse ap(argc,argv);  // parse the arguments.
  if(ap.error())           // true if at least one arguments is unknown.
    return EXIT_FAILURE;
  if(ap.help())            // if true, show help message and exit.
    return EXIT_SUCCESS;
  Application application(ap.data);
  application.run();

  std::cout << "\nsee you soon! ;)\n";
  return EXIT_SUCCESS;
}
